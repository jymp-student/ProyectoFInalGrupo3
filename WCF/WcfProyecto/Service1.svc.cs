﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Services;
using WcfProyecto;

namespace WcfProyecto
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        private ERrestauranteContainer DB = new ERrestauranteContainer();

        string connString = System.Configuration.ConfigurationManager.ConnectionStrings["ERrestauranteContainer"].ConnectionString;
        SqlDataAdapter selectString;
        DataSet dataSet;

        [WebMethod]
        public DataSet StudentList()
        {
            SqlConnection connectionString = new SqlConnection(connString);
            selectString = new SqlDataAdapter("select * from OGRENCI", connectionString);
            dataSet = new DataSet();
            selectString.Fill(dataSet);
            return dataSet;
        }




        public string ConsultarLogin(string Id, string Password)
        {
            try
            {
                int aux = Convert.ToInt16(Id);
                string a = Convert.ToString(DB.USUARIOSet.Where(p => p.Id == aux).First().Password);
                if (Password==a)
                {
                    string b = Convert.ToString(DB.USUARIOSet.Where(p => p.Id == aux).First().Cargo);
                    return b;
                }
                else {
                return "Acceso Denegado";
                }
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }


        public List<MESA> MostrarMesas()
        {
           // return DB.MESASet.ToList();
            try
            {
                return DB.MESASet.ToList();
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }


        public MESA ConsultarMesa(string id)
        {
            try
            {
                int aux = Convert.ToInt16(id);
                return DB.MESASet.Where(p => p.Id == aux).First();
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }


        public List<PLATO> MostrarPlatos()
        {
            try
            {
                return DB.PLATOSet.ToList();
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public PLATO ConsultarPlato(string id)
        {
            try
            {
                int aux = Convert.ToInt16(id);
                return DB.PLATOSet.Where(p => p.Id == aux).First();
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }


        public List<PEDIDO> MostrarPedidos()
        {
            try
            {
                return DB.PEDIDOSet.ToList();
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public PEDIDO ConsultarPedido(string id)
        {
            try
            {
                int aux = Convert.ToInt16(id);
                return DB.PEDIDOSet.Where(p => p.Id == aux).First();
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }


        public void CrearPedido(PEDIDO x)
        {
            try
            {
                using (var db = new ERrestauranteContainer())
                {
                    db.PEDIDOSet.Add(x);
                    db.SaveChanges();
                }
                
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }


        public void ModidicarPedido(PEDIDO x)
        {
            try
            {
                using (var db = new ERrestauranteContainer())
                {
                    var model = db.PEDIDOSet.SingleOrDefault(y => y.Id == x.Id);
                    if (model == null)
                    {
                        throw new Exception();
                    }
                    model.Fecha = x.Fecha;
                    model.Estado = x.Estado;
                    model.USUARIOId = x.USUARIOId;
                    model.MESAId = x.MESAId;
                    if(db.ChangeTracker.HasChanges())
                    {
                        db.SaveChanges();
                    }
                }
               
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public void ModificarEstadoPedido(PEDIDO x)
        {
            try
            {
                using (var db = new ERrestauranteContainer())
                {
                    var model = db.PEDIDOSet.SingleOrDefault(y => y.Id == x.Id);
                    if (model == null)
                    {
                        throw new Exception();
                    }
                    model.Estado = x.Estado;
                    if (db.ChangeTracker.HasChanges())
                    {
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public List<INFORMACION> ConsultarInfo()
        {
            try
            {
                return DB.INFORMACIONSet.ToList();
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
            

        }
    }

  
}
