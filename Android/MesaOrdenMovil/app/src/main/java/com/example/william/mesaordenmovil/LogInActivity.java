package com.example.william.mesaordenmovil;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;


public class LogInActivity extends Activity {

    EditText etIdUsuario, etContrasenia;
    Button btLoginIngresar;
    StringBuilder stringBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        getScreenSize();
        etIdUsuario = (EditText) findViewById(R.id.etIdUsuario);
        etContrasenia = (EditText) findViewById(R.id.etContrasenia);
        btLoginIngresar = (Button) findViewById(R.id.btLoginIngresar);

    }
    public void changeToListaMesasActivity(String tipoUsuario){
        Intent intent=new Intent(this, MeseroMainActivity.class);
        intent.putExtra("tipoUsuario", tipoUsuario);
        startActivity(intent);
    }
    /*
    public void setMargins(){
        Point size = new Point();
        this.getWindowManager().getDefaultDisplay().getSize(size);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 200, 0, 0);

        etNombreUsuario.setLayoutParams(params);
    }
    */
    public void getScreenSize() {
        Point size = new Point();
        this.getWindowManager().getDefaultDisplay().getSize(size);
        int width = size.x;
        int height = size.y;
        //Toast.makeText(this, "Altura: "+height+". Ancho: "+width, Toast.LENGTH_SHORT).show();
    }

    public void authenticate(View view) {
        /*if(etIdUsuario.getText().toString()=="10"||etContrasenia.getText().toString().equals("1234")){

        }
        else if(etIdUsuario.getText().toString()=="11"||etContrasenia.getText().toString().equals("1234")){

        }
        else {
            Toast.makeText(this, "Id de Usuario o Contraseña incorrectos", Toast.LENGTH_SHORT).show();
        }
        */
        AuthenticationThread authenticationThread = new AuthenticationThread();
        authenticationThread.execute();
        //Toast.makeText(this, "El boton funciona", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_log_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class AuthenticationThread extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {
            try {
                String id=etIdUsuario.getText().toString();
                String password=etContrasenia.getText().toString();
                //URL url = new URL("http://caballerosdelchaira.com/wsjson/Service1.svc/mostrarUsuario/1");
                URL url = new URL("http://caballerosdelchaira.com/wsjson/Service1.svc/mostrarUsuario/1");
                //URL url = new URL("http://localhost:52785/Service1.svc/ConsultarLogin/1/1234");
                URLConnection jc = url.openConnection();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(jc.getInputStream()));
                String line;
                stringBuilder=new StringBuilder();
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
            } catch (Exception e) {
                Log.d("Error", e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            try {
                final JSONObject jsonObject = new JSONObject(stringBuilder.toString());
                Toast.makeText(getBaseContext(), "id: " + jsonObject.getString("Id") +
                        "Nombre: " + jsonObject.getString("Name"), Toast.LENGTH_SHORT).show();
                if(jsonObject.getString("Id").equals(etIdUsuario.getText().toString())){
                    //changeToListaMesasActivity(jsonObject.getString("mesero"));
                    changeToListaMesasActivity("mesero");
                }
            } catch (JSONException je) {

            } catch (Exception e) {

            }

        }
    }
}
