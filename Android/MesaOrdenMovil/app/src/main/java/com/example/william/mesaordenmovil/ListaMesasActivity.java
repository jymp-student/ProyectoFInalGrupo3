package com.example.william.mesaordenmovil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by William on 29/07/2015.
 */
public class ListaMesasActivity extends Activity{
    ListView lvMesas;
    TextView tvTipoUsuarioLM;
    String tipoUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_mesas);
        lvMesas=(ListView)findViewById(R.id.lvMesas);
        tvTipoUsuarioLM=(TextView)findViewById(R.id.tvTipoUsuarioLM);
        lvMesas.getBackground().setAlpha(200);
        Bundle extras=getIntent().getExtras();
        tipoUsuario=extras.getString("tipoUsuario");
        tvTipoUsuarioLM.setText(tipoUsuario);

        String[] mesas={"m1","m2","m3","m4","m5", "m6", "m7", "m8", "m9"};
        final ArrayList<String> arrayList=new ArrayList<String>();
        for (int i=0; i<mesas.length; i++){
            arrayList.add(mesas[i]);
        }
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mesas);
        lvMesas.setAdapter(adapter);
        lvMesas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String s=arrayList.get(position);
                Toast.makeText(getBaseContext(), "Mesa seleccionada: "+s, Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getBaseContext(), Mesa_Activity.class);
                intent.putExtra("mesa",s);
                intent.putExtra("tipoUsuario", tipoUsuario);
                startActivity(intent);
            }
        });
    }


}
