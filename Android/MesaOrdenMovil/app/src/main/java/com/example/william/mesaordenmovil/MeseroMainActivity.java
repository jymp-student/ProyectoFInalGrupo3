package com.example.william.mesaordenmovil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by William on 05/08/2015.
 */
public class MeseroMainActivity extends Activity{
    TextView tvTipoUsuarioMM;
    ImageButton ibMostrarMesas, ibMostrarPedidos;
    String tipoUsuario;
    Bundle extras;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mesero_main);
        ibMostrarMesas=(ImageButton)findViewById(R.id.ibMostrarMesas);
        ibMostrarPedidos=(ImageButton)findViewById(R.id.ibMostrarPedidos);
        tvTipoUsuarioMM=(TextView)findViewById(R.id.tvTipoUsuarioMM);
        extras=getIntent().getExtras();
        tipoUsuario=extras.getString("tipoUsuario");
        tvTipoUsuarioMM.setText(tipoUsuario);

    }

    public void onClickMostrarMesas(View view){

        Intent intent=new Intent(this, ListaMesasActivity.class);
        intent.putExtra("tipoUsuario", tipoUsuario);
        startActivity(intent);
    }
    public void onClickMostrarPedidos(View view){
        Intent intent=new Intent(this, ListaPedidosActivity.class);
        intent.putExtra("tipoUsuario", tipoUsuario);
        startActivity(intent);
    }
}
