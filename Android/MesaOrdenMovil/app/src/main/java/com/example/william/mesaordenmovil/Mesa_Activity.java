package com.example.william.mesaordenmovil;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by William on 03/08/2015.
 */
public class Mesa_Activity extends Activity {
    TextView tvNumeroMesa, tvCantidad, tvTipoUsuarioMesa;
    Button btKey0, btKey1, btKey2, btKey3, btKey4, btKey5, btKey6, btKey7, btKey8, btKey9;
    Button btLimpiar;
    TextView tvPedido;
    Button btCrearPedido, btModificarPedido;
    GridView gvPlatos;
    String[] platos={"Ajiaco", "Bandeja Paisa", "Ejecutivo","Mixto de Carnes","Sancocho"};
    String textoPrueba="Este es un texto muy largo en el que el proposito es escribir muchas lineas, para poder comprobar como ocurre el desplazamiento del texto y asi saber si es necesario insertar una vista de tipo scroll, a menos que estas cajas de texto las incluyan por defecto. Dependiendo del resultado se optara por dejarlo como está o buscar otra solucion.";
    String tipoUsuario;
    String cantidad="";
    String pedido="";
    String estadoMesa="disponible";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mesa);
        tvNumeroMesa=(TextView)findViewById(R.id.tvNumeroMesa);
        tvCantidad=(TextView)findViewById(R.id.tvCantidad);
        tvTipoUsuarioMesa=(TextView)findViewById(R.id.tvTipoUsuarioMesa);
        btKey0=(Button)findViewById(R.id.btkey0); btKey1=(Button)findViewById(R.id.btKey1);
        btKey2=(Button)findViewById(R.id.btKey2); btKey3=(Button)findViewById(R.id.btKey3);
        btKey4=(Button)findViewById(R.id.btKey4); btKey5=(Button)findViewById(R.id.btKey5);
        btKey6=(Button)findViewById(R.id.btKey6); btKey7=(Button)findViewById(R.id.btKey7);
        btKey8=(Button)findViewById(R.id.btKey8); btKey9=(Button)findViewById(R.id.btKey9);
        btLimpiar=(Button)findViewById(R.id.btLimpiar);
        btCrearPedido=(Button)findViewById(R.id.btCrearPedido);
        btModificarPedido=(Button)findViewById(R.id.btModificarPedido);
        tvPedido=(TextView)findViewById(R.id.tvPedido);
        gvPlatos=(GridView)findViewById(R.id.gvPlatos);
        tvPedido.setMovementMethod(ScrollingMovementMethod.getInstance());
        cargarEstadoMesa();
        //tvPedido.setText(textoPrueba);
        final ArrayList<String> arrayList=new ArrayList<String>();
        for (int i=0; i<platos.length; i++){
            arrayList.add(platos[i]);
        }
        ArrayAdapter adapter=new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);
        gvPlatos.setAdapter(adapter);
        Bundle extras=getIntent().getExtras();
        tipoUsuario=extras.getString("tipoUsuario");
        tvNumeroMesa.setText(extras.getString("mesa"));
        tvTipoUsuarioMesa.setText(tipoUsuario);
        gvPlatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String auxiliar=arrayList.get(position);
                if(cantidad.equals("")){
                    cantidad="1";
                }
                pedido+=cantidad+"  "+auxiliar+"\n";
                tvPedido.setText(pedido);
                cantidad="";
                actualizarCantidad();
            }
        });
    }
    public void cargarEstadoMesa(){
        if(estadoMesa.equals("disponible")){
            btModificarPedido.setVisibility(View.GONE);
        }
        else if(estadoMesa.equals("ocupada")){
            btCrearPedido.setVisibility(View.VISIBLE);
        }
    }
    public void crearPedido(View view){

    }
    public void modificarPedido(View view){

    }
    public void onClickBtKey0(View view){
        cantidad+=String.valueOf(0);
        actualizarCantidad();
    }
    public void onClickBtKey1(View view){
        cantidad+=String.valueOf(1);
        actualizarCantidad();
    }
    public void onClickBtKey2(View view){
        cantidad+=String.valueOf(2);
        actualizarCantidad();
    }
    public void onClickBtKey3(View view){
        cantidad+=String.valueOf(3);
        actualizarCantidad();
    }
    public void onClickBtKey4(View view){
        cantidad+=String.valueOf(4);
        actualizarCantidad();
    }
    public void onClickBtKey5(View view){
        cantidad+=String.valueOf(5);
        actualizarCantidad();
    }
    public void onClickBtKey6(View view){
        cantidad+=String.valueOf(6);
        actualizarCantidad();
    }
    public void onClickBtKey7(View view){
        cantidad+=String.valueOf(7);
        actualizarCantidad();
    }
    public void onClickBtKey8(View view){
        cantidad+=String.valueOf(8);
        actualizarCantidad();
    }
    public void onClickBtKey9(View view){
        cantidad+=String.valueOf(9);
        actualizarCantidad();
    }
    public void onClickLimpiar(View view){
        cantidad="";
        actualizarCantidad();
    }
    public void actualizarCantidad(){
        tvCantidad.setText(cantidad);
    }
}
